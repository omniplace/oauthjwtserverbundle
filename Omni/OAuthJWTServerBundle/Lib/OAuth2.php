<?php

namespace Omni\OAuthJWTServerBundle\Lib;

use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OAuth2\IOAuth2GrantCode;
use OAuth2\IOAuth2GrantUser;
use OAuth2\IOAuth2RefreshTokens;
use OAuth2\Model\IOAuth2AccessToken;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2 as BaseOAuth2;
use OAuth2\OAuth2AuthenticateException;
use OAuth2\OAuth2ServerException;
use Omni\OAuthJWTServerBundle\Event\OAuthPayloadEvent;
use Omni\OAuthJWTServerBundle\Event\OAuthScopeEvent;
use Omni\OAuthJWTServerBundle\EventListener\JWTCreatedEventListener;
use Omni\OAuthJWTServerBundle\Model\AccessToken;
use Omni\OAuthJWTServerBundle\Model\ClientInterface;
use Omni\OAuthJWTServerBundle\Model\ClientManagerInterface;
use Omni\OAuthJWTServerBundle\Model\EmptyUser;
use Omni\OAuthJWTServerBundle\Traits\ScopeTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Override base OAuth lib class for implement JWT integration.
 *
 * Class OAuth2
 * @package Omni\OAuthJWTServerBundle\Lib
 */
class OAuth2 extends BaseOAuth2
{
    use ScopeTrait;

    /**
     * JWT payload identifiers.
     */

    public const SCOPE_IDENTIFIER = 'sc';
    public const CLIENT_ID_IDENTIFIER = 'cl_id';
    public const GRANT_TYPE_IDENTIFIER = 'gt';
    public const USERNAME_IDENTIFIER = 'un';
    public const USER_ID_IDENTIFIER = 'ui';

    /**
     * @var string
     */
    protected $grantType;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var $jwtManager JWTTokenManagerInterface
     */
    protected $jwtManager;

    /**
     * @var UserProviderInterface
     */
    protected $userProvider;

    /**
     * @var ClientManagerInterface
     */
    protected $clientManager;

    /**
     * @var JWTCreatedEventListener
     */
    protected $jwtListener;

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     */
    public function grantAccessToken(Request $request = null): Response
    {
        $filters = array(
          'grant_type' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => self::GRANT_TYPE_REGEXP),
            'flags' => FILTER_REQUIRE_SCALAR
          ),
          'scope' => array('flags' => FILTER_REQUIRE_SCALAR),
          'code' => array('flags' => FILTER_REQUIRE_SCALAR),
          'redirect_uri' => array('filter' => FILTER_SANITIZE_URL),
          'username' => array('flags' => FILTER_REQUIRE_SCALAR),
          'password' => array('flags' => FILTER_REQUIRE_SCALAR),
          'refresh_token' => array('flags' => FILTER_REQUIRE_SCALAR),
        );

        if ($request === null) {
            $request = Request::createFromGlobals();
        }

        // Input data by default can be either POST or GET
        if ($request->getMethod() === 'POST') {
            $inputData = $request->request->all();
        } else {
            $inputData = $request->query->all();
        }

        // Basic authorization header
        $authHeaders = $this->getAuthorizationHeader($request);

        // Filter input data
        $input = filter_var_array($inputData, $filters);

        $this->grantType = $input['grant_type'];

        // Grant Type must be specified.
        if (!$this->grantType) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Invalid grant_type parameter or parameter missing');
        }

        // Authorize the client
        $clientCredentials = $this->getClientCredentials($inputData, $authHeaders);

        $client = $this->storage->getClient($clientCredentials[0]);

        if (!$client) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
        }

        if ($this->storage->checkClientCredentials($client, $clientCredentials[1]) === false) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
        }

        if (!$this->storage->checkRestrictedGrantType($client, $this->grantType)) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_UNAUTHORIZED_CLIENT, 'The grant type is unauthorized for this client_id');
        }

        // Do the granting
        switch ($this->grantType) {
            case self::GRANT_TYPE_AUTH_CODE:
                // returns array('data' => data, 'scope' => scope)
                $stored = $this->grantAccessTokenAuthCode($client, $input);
                break;
            case self::GRANT_TYPE_USER_CREDENTIALS:
                // returns: true || array('scope' => scope)
                $stored = $this->grantAccessTokenUserCredentials($client, $input);
                break;
            case self::GRANT_TYPE_CLIENT_CREDENTIALS:
                // returns: true || array('scope' => scope)
                $stored = $this->grantAccessTokenClientCredentials($client, $input, $clientCredentials);
                $stored += ['data' => new EmptyUser()];
                break;
            case self::GRANT_TYPE_REFRESH_TOKEN:
                // returns array('data' => data, 'scope' => scope)
                $stored = $this->grantAccessTokenRefreshToken($client, $input);
                break;
            default:
                throw new OAuth2ServerException(
                  self::HTTP_BAD_REQUEST,
                  self::ERROR_INVALID_REQUEST,
                  'Invalid grant_type parameter or parameter missing'
                );
        }

        if (!\is_array($stored)) {
            $stored = [];
        }

        // if no scope provided to check against $input['scope'] then application defaults are set
        // if no data is provided than null is set
        $stored += array('scope' => $this->getVariable(self::CONFIG_SUPPORTED_SCOPES), 'data' => null,
          'access_token_lifetime' => $this->getVariable(self::CONFIG_ACCESS_LIFETIME),
          'issue_refresh_token' => true, 'refresh_token_lifetime' => $this->getVariable(self::CONFIG_REFRESH_LIFETIME));

        if (!$input['scope']) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_INVALID_SCOPE, 'Scope parameter missing');
        }

        if (!isset($stored['scope']) || !$this->checkScope($input['scope'], $stored['scope'])) {
            throw new OAuth2ServerException(Response::HTTP_BAD_REQUEST, self::ERROR_INVALID_SCOPE, 'An unsupported scope was requested.');
        }

        //dispatch event for extend scope checking
        $this->dispatcher->dispatch(OAuthScopeEvent::CHECK_SCOPE_ACCESS, new OAuthScopeEvent($stored['scope'], $input['scope'], $stored['data']));

        $scope = $input['scope'];

        $token = $this->createAccessToken($client, $stored['data'], $scope, $stored['access_token_lifetime'], $stored['issue_refresh_token'], $stored['refresh_token_lifetime']);

        return new Response(json_encode($token), Response::HTTP_OK, $this->getJsonHeaders());
    }

    /**
     * {@inheritdoc}
     *
     * @throws UsernameNotFoundException
     */
    public function verifyAccessToken($tokenParam, $scope = null): IOAuth2AccessToken
    {
        $tokenType = $this->getVariable(self::CONFIG_TOKEN_TYPE);
        $realm = $this->getVariable(self::CONFIG_WWW_REALM);

        if (!$tokenParam) { // Access token was not provided
            throw new OAuth2AuthenticateException(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_REQUEST, 'The request is missing a required parameter, includes an unsupported parameter or parameter value, repeats the same parameter, uses more than one method for including an access token, or is otherwise malformed.', $scope);
        }

        try {
            $payload = $this->jwtManager->decode($tokenParam);

            if (!$payload) {
                throw new OAuth2AuthenticateException(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The access token provided is invalid.', $scope);
            }

            $token = $this->buildToken($payload);

        } catch (JWTDecodeFailureException $e) {
            switch ($e->getReason()) {
                case JWTDecodeFailureException::INVALID_TOKEN:
                    throw new OAuth2AuthenticateException(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The access token provided is invalid.', $scope);
                case JWTDecodeFailureException::EXPIRED_TOKEN:
                    throw new OAuth2AuthenticateException(self::HTTP_UNAUTHORIZED, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The access token provided has expired.', $scope);
                case JWTDecodeFailureException::UNVERIFIED_TOKEN:
                    throw new OAuth2AuthenticateException(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The access token provided is invalid. Try to renew the token', $scope);
                default:
                    throw new OAuth2AuthenticateException(self::HTTP_BAD_REQUEST, $tokenType, $realm, self::ERROR_INVALID_GRANT, 'The access token provided is invalid.', $scope);
            }
        }

        if ($scope && !$this->checkScope($scope, $token->getScope())) {
            throw new OAuth2AuthenticateException(self::HTTP_FORBIDDEN, $tokenType, $realm, self::ERROR_INSUFFICIENT_SCOPE, 'The request requires higher privileges than provided by the access token.', $scope);
        }
        //dispatch event for extend scope checking
        $this->dispatcher->dispatch(OAuthScopeEvent::CHECK_SCOPE_ACCESS, new OAuthScopeEvent($token->getScope(), $scope, $token->getData()));

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function createAccessToken(IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        $token = array(
          'expires_in' => $access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME),
          'token_type' => $this->getVariable(self::CONFIG_TOKEN_TYPE),
          'scope' => $scope,
        );

        $token['access_token'] = $this->genJWT($data, $scope, $client);

        // Issue a refresh token also, if we support them
        if ($this->storage instanceof IOAuth2RefreshTokens && $issue_refresh_token === true) {
            $token['refresh_token'] = $this->genAccessToken();
            $this->storage->createRefreshToken(
              $token['refresh_token'],
              $client,
              $data,
              time() + ($refresh_token_lifetime ?: $this->getVariable(self::CONFIG_REFRESH_LIFETIME)),
              $scope
            );

            // If we've granted a new refresh token, expire the old one
            if (null !== $this->oldRefreshToken) {
                $this->storage->unsetRefreshToken($this->oldRefreshToken);
                $this->oldRefreshToken = null;
            }
        }

        if ($this->storage instanceof IOAuth2GrantCode && $this->usedAuthCode !== null) {
            $this->storage->markAuthCodeAsUsed($this->usedAuthCode->getToken());
            $this->usedAuthCode = null;
        }

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    protected function grantAccessTokenUserCredentials(IOAuth2Client $client, array $input)
    {
        if (!($this->storage instanceof IOAuth2GrantUser)) {
            throw new OAuth2ServerException(Response::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
        }

        if (!$input['username'] || !$input['password']) {
            throw new OAuth2ServerException(Response::HTTP_BAD_REQUEST, self::ERROR_INVALID_REQUEST, 'Missing parameters. "username" and "password" required');
        }

        $stored = $this->storage->checkUserCredentials($client, $input['username'], $input['password']);

        if (empty($stored)) {
            throw new OAuth2ServerException(Response::HTTP_BAD_REQUEST, self::ERROR_INVALID_GRANT, 'Invalid username and password combination');
        }

        return $stored;
    }

    /**
     * @param array $payload
     *
     * @return IOAuth2AccessToken
     * @throws JWTDecodeFailureException
     * @throws UsernameNotFoundException
     */
    protected function buildToken(array $payload): IOAuth2AccessToken
    {
        $token = new AccessToken();

        $token->setUser($this->getUserFromPayload($payload));
        $token->setScope($this->getScopeFromPayload($payload));
        $token->setClient($this->getClientFromPayload($payload));
        $token->setGrantType($this->getGrantTypeFromPayload($payload));

        $payloadEvent = new OAuthPayloadEvent($payload, $token);
        $this->dispatcher->dispatch(OAuthPayloadEvent::EXTRACT_PAYLOAD_EVENT, $payloadEvent);

        return $token;
    }

    /**
     * Generates an JWT token.
     *
     * @param UserInterface   $user
     * @param string          $scope
     * @param ClientInterface $client
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function genJWT($user, string $scope, ClientInterface $client): string
    {
        if (!$user instanceof UserInterface) {
            throw new \InvalidArgumentException('Invalid user supplied for JWT creation');
        }

        $payload = [
            self::SCOPE_IDENTIFIER => $this->parseScope($scope),
            self::GRANT_TYPE_IDENTIFIER => $this->getGrantType(),
            self::CLIENT_ID_IDENTIFIER => $client->getPublicId(),
            self::USERNAME_IDENTIFIER => $user->getUsername(),
        ];

        $this->jwtListener->setData($payload);

        return $this->jwtManager->create($user);
    }

    /**
     * Load user object from payload, by user identity.
     *
     * @param array $payload
     *
     * @return UserInterface|null

     * @throws \LogicException
     * @throws JWTDecodeFailureException
     * @throws UsernameNotFoundException
     */
    private function getUserFromPayload(array $payload): UserInterface
    {
        $username = $payload[self::USERNAME_IDENTIFIER] ?? null;
        $userId = $payload[self::USER_ID_IDENTIFIER] ?? null;

        if ($userId === null) {
            return new EmptyUser();
        }

        if (!$username) {
            $this->throwInvalidJWT();
        }

        $user = $this->userProvider->loadUserByUsername($username);

        if (!$user instanceof UserInterface) {
            throw new \LogicException('Invalid user object was returned from user provider.');
        }

        return $user;
    }

    /**
     * Get scope from payload.
     *
     * @param array $payload
     *
     * @return string
     *
     * @throws JWTDecodeFailureException
     */
    private function getScopeFromPayload(array $payload): string
    {
        if (!isset($payload[self::SCOPE_IDENTIFIER])) {
            $this->throwInvalidJWT();
        }

        return $payload[self::SCOPE_IDENTIFIER];
    }

    /**
     * Get client object from payload.
     *
     * @param array $payload
     *
     * @return ClientInterface
     *
     * @throws JWTDecodeFailureException
     */
    private function getClientFromPayload(array $payload): ClientInterface
    {
        if (!isset($payload[self::CLIENT_ID_IDENTIFIER])) {
            $this->throwInvalidJWT();
        }

        $client = $this->clientManager->findClientByPublicId($payload[self::CLIENT_ID_IDENTIFIER]);

        if ($client === null) {
            $this->throwInvalidJWT();
        }

        return $client;
    }

    /**
     * Get grant type from payload.
     *
     * @param array $payload
     *
     * @return string
     *
     * @throws JWTDecodeFailureException
     */
    private function getGrantTypeFromPayload(array $payload): string
    {
        if (!isset($payload[self::GRANT_TYPE_IDENTIFIER])) {
            $this->throwInvalidJWT();
        }

        return $payload[self::GRANT_TYPE_IDENTIFIER];
    }

    /**
     * Copy from parent methods.
     *
     * @see BaseOAuth2::getJsonHeaders()
     */
    private function getJsonHeaders()
    {
        $headers = $this->getVariable(self::CONFIG_RESPONSE_EXTRA_HEADERS, array());
        $headers += array(
          'Content-Type' => 'application/json',
          'Cache-Control' => 'no-store',
          'Pragma' => 'no-cache',
        );
        return $headers;
    }

    /**
     * Before token creation a grant_type may can to set in 2 points:
     *      - grantAccessToken method
     *      - AuthorizationController class
     * If the token is received via grantAccessToken method we can to set
     * this class property, otherwise it is implicit grant type.
     *
     * @return string Grant type.
     */
    private function getGrantType(): string
    {
        return $this->grantType ?? self::GRANT_TYPE_IMPLICIT;
    }

    /**
     * Throw invalid Json Web Token exception.
     *
     * @throws JWTDecodeFailureException
     */
    private function throwInvalidJWT(): void
    {
        throw new JWTDecodeFailureException(JWTDecodeFailureException::INVALID_TOKEN, 'Invalid JWT Token');
    }
}
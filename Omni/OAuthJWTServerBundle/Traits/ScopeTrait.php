<?php

namespace Omni\OAuthJWTServerBundle\Traits;

trait ScopeTrait
{
    public function inlineScope(array $scope): string
    {
        return implode(' ', $scope);
    }

    public function parseScope(string $scope): array
    {
        return explode(' ', $scope);
    }
}
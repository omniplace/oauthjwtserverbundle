<?php

namespace Omni\OAuthJWTServerBundle\Controller;

use OAuth2\OAuth2ServerException;
use Omni\OAuthJWTServerBundle\Event\OAuthEvent;
use Omni\OAuthJWTServerBundle\Form\Handler\AuthorizeFormHandler;
use Omni\OAuthJWTServerBundle\Lib\OAuth2;
use Omni\OAuthJWTServerBundle\Model\ClientInterface;
use Omni\OAuthJWTServerBundle\Model\ClientManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthorizeController
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Form
     */
    private $authorizeForm;

    /**
     * @var AuthorizeFormHandler
     */
    private $authorizeFormHandler;

    /**
     * @var OAuth2
     */
    private $oAuth2Server;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var UrlGeneratorInterface
     */
    private $route;

    /**
     * @var ClientManagerInterface
     */
    private $clientManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AuthorizeController constructor.
     *
     * @param ClientInterface          $client
     * @param SessionInterface         $session
     * @param Form                     $authorizeForm
     * @param AuthorizeFormHandler     $authorizeFormHandler
     * @param OAuth2                   $oAuth2Server
     * @param EngineInterface          $templating
     * @param RequestStack             $requestStack
     * @param TokenStorageInterface    $tokenStorage
     * @param UrlGeneratorInterface    $route
     * @param ClientManagerInterface   $clientManager
     * @param string                   $templateEngineType
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
      ClientInterface $client,
      Form $authorizeForm,
      AuthorizeFormHandler $authorizeFormHandler,
      OAuth2 $oAuth2Server,
      EngineInterface $templating,
      RequestStack $requestStack,
      TokenStorageInterface $tokenStorage,
      UrlGeneratorInterface $route,
      ClientManagerInterface $clientManager,
      EventDispatcherInterface $eventDispatcher,
      SessionInterface $session = null
    ) {
        $this->client = $client;
        $this->session = $session;
        $this->authorizeForm = $authorizeForm;
        $this->authorizeFormHandler = $authorizeFormHandler;
        $this->oAuth2Server = $oAuth2Server;
        $this->templating = $templating;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->route = $route;
        $this->clientManager = $clientManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Authorize
     *
     * @Route(path="/auth/oauth/v2/authorize", name="omni_oauth_jwt_server_authorize")
     *
     * @throws \RuntimeException
     * @throws \OAuth2\OAuth2RedirectException
     */
    public function authorizeAction(Request $request): Response
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($this->session && $this->session->get('_omni_oauth_jwt_server.ensure_logout')) {
            $this->session->invalidate(600);
            $this->session->get('_omni_oauth_jwt_server.ensure_logout');
        }

        $form = $this->authorizeForm;
        $formHandler = $this->authorizeFormHandler;

        $event = $this->eventDispatcher->dispatch(
            OAuthEvent::PRE_AUTHORIZATION_PROCESS,
            new OAuthEvent($user, $this->getClient($request))
        );

        if ($event->isAuthorizedClient()) {
            $scope = $request->get('scope', null);

            return $this->oAuth2Server->finishClientAuthorization(true, $user, $request, $scope);
        }

        if ($formHandler->process()) {
            return $this->processSuccess($user, $formHandler, $request);
        }

        return $this->templating->renderResponse(
          'OmniOAuthJWTServerBundle:Authorize:authorize.html.twig',
          [
              'form'   => $form->createView(),
              'client' => $this->getClient($request),
          ]
        );
    }

    /**
     * @param UserInterface        $user
     * @param AuthorizeFormHandler $formHandler
     * @param Request              $request
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    protected function processSuccess(UserInterface $user, AuthorizeFormHandler $formHandler, Request $request): ?Response
    {
        if ($this->session && true === $this->session->get('_fos_oauth_server.ensure_logout')) {
            $this->tokenStorage->setToken(null);
            $this->session->invalidate();
        }

        $this->eventDispatcher->dispatch(
          OAuthEvent::POST_AUTHORIZATION_PROCESS,
          new OAuthEvent($user, $this->getClient($request), $formHandler->isAccepted())
        );

        $formName = $this->authorizeForm->getName();
        if (!$request->query->all() && $request->request->has($formName)) {
            $request->query->add($request->request->get($formName));
        }

        try {
            return $this->oAuth2Server->finishClientAuthorization($formHandler->isAccepted(), $user, $request, $formHandler->getScope());
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        }
    }

    /**
     * @param Request $request
     *
     * @return ClientInterface.
     *
     * @throws NotFoundHttpException
     */
    protected function getClient(Request $request)
    {
        if (null !== $this->client) {
            return $this->client;
        }

        if (null === $clientId = $request->get('client_id')) {
            $formData = $request->get($this->authorizeForm->getName(), []);
            $clientId = $formData['client_id'] ?? null;
        }

        $this->client = $this->clientManager->findClientByPublicId($clientId);

        if (null === $this->client) {
            throw new NotFoundHttpException('Client not found.');
        }

        return $this->client;
    }
}
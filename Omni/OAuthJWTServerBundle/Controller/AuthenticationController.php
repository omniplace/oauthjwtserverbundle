<?php

namespace Omni\OAuthJWTServerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthenticationController
{
    /**
     * @var AuthenticationUtils
     */
    private $authUtils;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * AuthenticationController constructor.
     *
     * @param AuthenticationUtils $authUtils
     * @param EngineInterface     $templating
     */
    public function __construct(AuthenticationUtils $authUtils, EngineInterface $templating)
    {
        $this->authUtils = $authUtils;
        $this->templating = $templating;
    }

    /**
     * @Route(path="/auth", name="login")
     *
     * @throws \RuntimeException
     */
    public function loginAction(Request $request): Response
    {
        $error = $this->authUtils->getLastAuthenticationError();
        $lastUsername = $this->authUtils->getLastUsername();

        return $this->templating->renderResponse('@OmniOAuthJWTServer/Login/login.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
}
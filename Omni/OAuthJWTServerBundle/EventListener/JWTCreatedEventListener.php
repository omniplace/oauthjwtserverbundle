<?php

namespace Omni\OAuthJWTServerBundle\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Omni\OAuthJWTServerBundle\Event\OAuthPayloadEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class JWTCreatedEventListener
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * JWTCreatedEventListener constructor.
     */
    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @throws \LogicException
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();

        $payload += $this->data;

        $payloadEvent = new OAuthPayloadEvent($payload);
        $this->dispatcher->dispatch(OAuthPayloadEvent::ADD_PAYLOAD_EVENT, $payloadEvent);

        $event->setData($payloadEvent->getPayload());
    }
}
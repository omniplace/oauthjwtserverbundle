<?php

namespace Omni\OAuthJWTServerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\OutOfBoundsException;
use Symfony\Component\DependencyInjection\Reference;

class TokenStorageCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws OutOfBoundsException
     */
    public function process(ContainerBuilder $container): void
    {
        $definition = $container->getDefinition('omni_oauth_jwt_server.security.authentication.listener');

        if ($container->hasDefinition('security.token_storage') === false) {
            $definition->replaceArgument(0, new Reference('security.context'));
        }
    }
}
<?php

namespace Omni\OAuthJWTServerBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OAuthFactory implements SecurityFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint): array
    {
        $providerId = 'security.authentication.provider.omni_oauth_jwt_server.'.$id;
        $definition = new ChildDefinition('omni_oauth_jwt_server.security.authentication.provider');

        $container
          ->setDefinition($providerId, $definition)
          ->replaceArgument(0, new Reference($userProvider))
        ;

        $listenerId = 'security.authentication.listener.omni_oauth_jwt_server.'.$id;
        $definition = new ChildDefinition('omni_oauth_jwt_server.security.authentication.listener');

        $container->setDefinition($listenerId, $definition);

        return [$providerId, $listenerId, 'omni_oauth_jwt_server.security.entry_point'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition(): string
    {
        return 'pre_auth';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey(): string
    {
        return 'oauth_jwt';
    }

    /**
     * {@inheritdoc}
     */
    public function addConfiguration(NodeDefinition $builder): void
    {
    }
}
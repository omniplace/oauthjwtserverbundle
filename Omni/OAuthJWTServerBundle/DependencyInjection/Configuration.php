<?php

namespace Omni\OAuthJWTServerBundle\DependencyInjection;

use Omni\OAuthJWTServerBundle\Lib\OAuth2;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_oauth_jwt_server');

        $rootNode
            ->children()
//                ->scalarNode('client_class')->isRequired()->cannotBeEmpty()->end()
//                ->scalarNode('refresh_token_class')->isRequired()->cannotBeEmpty()->end()
//                ->scalarNode('auth_code_class')->isRequired()->cannotBeEmpty()->end()
            ->end()
        ;

        $this->addAuthorizeSection($rootNode);
        $this->addServiceSection($rootNode);
        $this->addTokenSection($rootNode);

        return $treeBuilder;
    }

    private function addAuthorizeSection(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('authorize')
                    ->addDefaultsIfNotSet()
                    ->canBeUnset()
                    ->children()
                        ->arrayNode('form')
                            ->children()
                                ->scalarNode('type')->defaultValue('omni_oauth_jwt_server_authorize')->end()
                                ->scalarNode('handler')->defaultValue('omni_oauth_jwt_server.authorize.form.handler.default')->end()
                                ->scalarNode('name')->defaultValue('omni_oauth_jwt_server_authorize_form')->end()
                                ->arrayNode('validation_groups')
                                    ->prototype('scalar')->end()
                                    ->defaultValue(['Authorize', 'Default'])
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addServiceSection(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('service')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('storage')->defaultValue('omni_oauth_jwt_server.storage.default')->cannotBeEmpty()->end()
                            ->scalarNode('user_provider')->defaultNull()->end()
                            ->scalarNode('client_manager')->defaultValue('omni_oauth_jwt_server.client_manager.default')->end()
                            ->scalarNode('refresh_token_manager')->defaultValue('omni_oauth_jwt_server.refresh_token_manager.default')->end()
                            ->scalarNode('auth_code_manager')->defaultValue('omni_oauth_jwt_server.auth_code_manager.default')->end()
                            ->arrayNode('options')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('token_type')->defaultValue(OAuth2::TOKEN_TYPE_BEARER)->end()
                                    ->scalarNode('realm')->defaultValue(OAuth2::DEFAULT_WWW_REALM)->end()
                                    ->scalarNode('enforce_redirect')->defaultValue(OAuth2::CONFIG_ENFORCE_INPUT_REDIRECT)->end()
                                    ->scalarNode('enforce_state')->defaultValue(OAuth2::CONFIG_ENFORCE_STATE)->end()
                                    ->arrayNode('response_extra_headers')
                                        ->scalarPrototype()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addTokenSection(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('token')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('supported_scopes')->defaultNull()->end()
                            ->arrayNode('access_token')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('ttl')->defaultValue(OAuth2::DEFAULT_ACCESS_TOKEN_LIFETIME)->end()
                                    ->scalarNode('private_key_path')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('public_key_path')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('pass_phrase')->defaultValue('')->end()
                                ->end()
                            ->end()
                            ->arrayNode('refresh_token')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('ttl')->defaultValue(OAuth2::DEFAULT_REFRESH_TOKEN_LIFETIME)->end()
                                ->end()
                            ->end()
                            ->arrayNode('auth_code')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('ttl')->defaultValue(OAuth2::DEFAULT_AUTH_CODE_LIFETIME)->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}

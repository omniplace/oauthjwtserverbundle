<?php

namespace Omni\OAuthJWTServerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class OmniOAuthJWTServerExtension extends Extension
{
    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('jwt.yml');
        $loader->load('manager.yml');
        $loader->load('authenticate.yml');

        $options['access_token_lifetime'] = $config['token']['access_token']['ttl'];
        $options['refresh_token_lifetime'] = $config['token']['refresh_token']['ttl'];
        $options['auth_code_lifetime'] = $config['token']['auth_code']['ttl'];
        $options['supported_scopes'] = $config['token']['supported_scopes'];
        $options['token_type'] = $config['service']['options']['token_type'];
        $options['realm'] = $config['service']['options']['realm'];
        $options['enforce_redirect'] = $config['service']['options']['enforce_redirect'];
        $options['enforce_state'] = $config['service']['options']['enforce_state'];
        $options['response_extra_headers'] = $config['service']['options']['response_extra_headers'];

        $container->setParameter('omni_oauth_jwt_server.server.options', $options);

        $container->setAlias('omni_oauth_jwt_server.storage', $config['service']['storage']);
        $container->setAlias('omni_oauth_jwt_server.client_manager', $config['service']['client_manager']);
        $container->setAlias('omni_oauth_jwt_server.refresh_token_manager', $config['service']['refresh_token_manager']);
        $container->setAlias('omni_oauth_jwt_server.auth_code_manager', $config['service']['auth_code_manager']);

        if (null !== $config['service']['user_provider']) {
            $container->setAlias('omni_oauth_jwt_server.user_provider', new Alias($config['service']['user_provider'], false));
        }

        $this->remapParametersNamespaces($config, $container, [
          '' => [
            'client_class' => 'omni_oauth_jwt_server.model.client_class',
            'refresh_token_class' => 'omni_oauth_jwt_server.model.refresh_token_class',
            'auth_code_class' => 'omni_oauth_jwt_server.model.auth_code_class',
          ],
          'template' => 'omni_oauth_jwt_server.template.%s',
        ]);

        if (!empty($config['authorize'])) {
            $this->loadAuthorize($config['authorize'], $container, $loader);

            $authorizeFormDefinition = $container->getDefinition('omni_oauth_jwt_server.authorize.form');
            $authorizeFormDefinition->setFactory([new Reference('form.factory'), 'createNamed']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return 'omni_oauth_jwt_server';
    }

    protected function remapParameters(array $config, ContainerBuilder $container, array $map)
    {
        foreach ($map as $name => $paramName) {
            if (array_key_exists($name, $config)) {
                $container->setParameter($paramName, $config[$name]);
            }
        }
    }

    protected function remapParametersNamespaces(array $config, ContainerBuilder $container, array $namespaces)
    {
        foreach ($namespaces as $ns => $map) {
            if ($ns) {
                if (!\array_key_exists($ns, $config)) {
                    continue;
                }
                $namespaceConfig = $config[$ns];
            } else {
                $namespaceConfig = $config;
            }

            if (\is_array($map)) {
                $this->remapParameters($namespaceConfig, $container, $map);
            } else {
                foreach ($namespaceConfig as $name => $value) {
                    $container->setParameter(sprintf($map, $name), $value);
                }
            }
        }
    }

    protected function loadAuthorize(array $config, ContainerBuilder $container, Loader\YamlFileLoader $loader): void
    {
        $loader->load('authorize.yml');

        $container->setAlias('omni_oauth_jwt_server.authorize.form.handler', $config['form']['handler']);
        unset($config['form']['handler']);

        $this->remapParametersNamespaces($config, $container, [
            'form' => 'omni_oauth_jwt_server.authorize.form.%s',
        ]);
    }
}

<?php

namespace Omni\OAuthJWTServerBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class OAuthToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $token;

    /**
     * @return mixed
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return $this->token;
    }
}
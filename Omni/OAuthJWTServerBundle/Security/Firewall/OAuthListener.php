<?php

namespace Omni\OAuthJWTServerBundle\Security\Firewall;

use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Omni\OAuthJWTServerBundle\Security\Authentication\Token\OAuthToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class OAuthListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $securityContext;

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @var OAuth2
     */
    protected $serverService;

    /**
     * OAuthListener constructor.
     *
     * @param TokenStorageInterface          $securityContext
     * @param AuthenticationManagerInterface $authenticationManager
     * @param OAuth2                         $serverService
     */
    public function __construct(TokenStorageInterface $securityContext, AuthenticationManagerInterface $authenticationManager, OAuth2 $serverService)
    {
        $this->securityContext = $securityContext;
        $this->authenticationManager = $authenticationManager;
        $this->serverService = $serverService;
    }


    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        if (null === $oauthToken = $this->serverService->getBearerToken($event->getRequest(), true)) {
            return;
        }

        $token = new OAuthToken();
        $token->setToken($oauthToken);

        try {
            $result = $this->authenticationManager->authenticate($token);

            if ($result instanceof TokenInterface) {
                return $this->securityContext->setToken($result);
            }

            if ($result instanceof Response) {
                return $event->setResponse($result);
            }
        } catch (AuthenticationException $e) {
            if (null !== $p = $e->getPrevious()) {
                $event->setResponse($p->getHttpResponse());
            }
        }
    }
}
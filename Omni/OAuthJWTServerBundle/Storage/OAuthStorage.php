<?php

namespace Omni\OAuthJWTServerBundle\Storage;

use OAuth2\Model\IOAuth2Client;
use Omni\OAuthJWTServerBundle\Model\AuthCodeInterface;
use Omni\OAuthJWTServerBundle\Model\AuthCodeManagerInterface;
use Omni\OAuthJWTServerBundle\Model\ClientInterface;
use Omni\OAuthJWTServerBundle\Model\ClientManagerInterface;
use Omni\OAuthJWTServerBundle\Model\ExtendedOAuthStorageInterface;
use Omni\OAuthJWTServerBundle\Model\RefreshTokenManagerInterface;
use Omni\OAuthJWTServerBundle\Model\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OAuthStorage implements ExtendedOAuthStorageInterface
{
    /**
     * @var ClientManagerInterface
     */
    protected $clientManager;

    /**
     * @var RefreshTokenManagerInterface
     */
    protected $refreshTokenManager;

    /**
     * @var AuthCodeManagerInterface
     */
    protected $authCodeManager;

    /**
     * @var UserProviderInterface
     */
    protected $userProvider;

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;
    
    
    public function __construct(ClientManagerInterface $cm, RefreshTokenManagerInterface $rtm,
        AuthCodeManagerInterface $acm, UserProviderInterface $up = null, EncoderFactoryInterface $ef = null)
    {
        $this->clientManager = $cm;
        $this->refreshTokenManager = $rtm;
        $this->authCodeManager = $acm;
        $this->userProvider = $up;
        $this->encoderFactory = $ef;
    }

    /**
     * {@inheritdoc}
     */
    public function checkClientCredentialsGrant(IOAuth2Client $client, $clientSecret): bool
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        return $client->checkSecret($clientSecret);
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthCode($code): ?AuthCodeInterface
    {
        return $this->authCodeManager->findAuthCodeByToken($code);
    }

    /**
     * {@inheritdoc}
     */
    public function createAuthCode($code, IOAuth2Client $client, $data, $redirectUri, $expires, $scope = null): AuthCodeInterface
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        $authCode = $this->authCodeManager->createAuthCode();
        $authCode->setToken($code);
        $authCode->setClient($client);
        $authCode->setUser($data);
        $authCode->setRedirectUri($redirectUri);
        $authCode->setExpiresAt($expires);
        $authCode->setScope($scope);
        $this->authCodeManager->updateAuthCode($authCode);

        return $authCode;
    }

    /**
     * {@inheritdoc}
     */
    public function markAuthCodeAsUsed($code): void
    {
        $authCode = $this->authCodeManager->findAuthCodeByToken($code);

        if (null !== $authCode) {
            $this->authCodeManager->deleteAuthCode($authCode);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkUserCredentials(IOAuth2Client $client, $username, $password): array
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        try {
            $user = $this->userProvider->loadUserByUsername($username);
        } catch (AuthenticationException $e) {
            return [];
        }

        if (null !== $user) {
            $encoder = $this->encoderFactory->getEncoder($user);

            if ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
                return ['data' => $user];
            }
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getRefreshToken($refreshToken): ?TokenInterface
    {
        return $this->refreshTokenManager->findTokenByToken($refreshToken);
    }

    /**
     * {@inheritdoc}
     */
    public function createRefreshToken($refreshToken, IOAuth2Client $client, $data, $expires, $scope = null): TokenInterface
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        $token = $this->refreshTokenManager->createToken();
        $token->setToken($refreshToken);
        $token->setClient($client);
        $token->setExpiresAt($expires);
        $token->setScope($scope);

        if (null !== $data) {
            $token->setUser($data);
        }

        $this->refreshTokenManager->updateToken($token);

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function unsetRefreshToken($refreshToken): void
    {
        $token = $this->refreshTokenManager->findTokenByToken($refreshToken);

        if (null !== $token) {
            $this->refreshTokenManager->deleteToken($token);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getClient($clientId): ?ClientInterface
    {
        return $this->clientManager->findClientByPublicId($clientId);
    }

    /**
     * {@inheritdoc}
     */
    public function checkClientCredentials(IOAuth2Client $client, $clientSecret = null): bool
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        return $client->checkSecret($clientSecret);
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessToken($oauthToken): ?TokenInterface
    {
        //OAuthJWTServerBundle not supports database storage for access token.
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function createAccessToken($oauthToken, IOAuth2Client $client, $data, $expires, $scope = null): ?TokenInterface
    {
        //OAuthJWTServerBundle not supports database storage for access token.
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function checkRestrictedGrantType(IOAuth2Client $client, $grantType): bool
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        return \in_array($grantType, $client->getAllowedGrantTypes(), true);
    }
}
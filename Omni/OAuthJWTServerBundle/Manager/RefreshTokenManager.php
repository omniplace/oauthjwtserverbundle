<?php

namespace Omni\OAuthJWTServerBundle\Manager;


use Omni\OAuthJWTServerBundle\Model\RefreshTokenManagerInterface;

class RefreshTokenManager extends TokenManager implements RefreshTokenManagerInterface
{
}
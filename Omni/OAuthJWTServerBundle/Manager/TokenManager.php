<?php

namespace Omni\OAuthJWTServerBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Omni\OAuthJWTServerBundle\Model\TokenInterface;
use Omni\OAuthJWTServerBundle\Model\TokenManagerInterface;

class TokenManager implements TokenManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * TokenManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param string                 $class
     */
    public function __construct(EntityManagerInterface $em, string $class)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $this->class = $class;
    }

    /**
     * Create a new TokenInterface.
     *
     * @return TokenInterface
     */
    public function createToken(): TokenInterface
    {
        return new $this->class;
    }

    /**
     * Return the class name of the Token.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Retrieve a token using a set of criteria.
     *
     * @param array $criteria
     *
     * @return TokenInterface|null
     */
    public function findTokenBy(array $criteria): ?TokenInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Retrieve a token (object) by its token string.
     *
     * @param string $token A token.
     *
     * @return TokenInterface|null
     */
    public function findTokenByToken(string $token): ?TokenInterface
    {
        return $this->findTokenBy(['token' => $token]);
    }

    /**
     * Save or update a given token.
     *
     * @param TokenInterface $token The token to save or update.
     */
    public function updateToken(TokenInterface $token): void
    {
        if (null === $token->getId()) {
            $this->em->persist($token);
        }

        $this->em->flush();
    }

    /**
     * Delete a given token.
     *
     * @param TokenInterface $token The token to delete.
     */
    public function deleteToken(TokenInterface $token): void
    {
        $this->em->remove($token);
        $this->em->flush();
    }

    /**
     * Delete expired tokens.
     *
     * @return int The number of tokens deleted.
     */
    public function deleteExpired(): int
    {
        return $this->repository->createQueryBuilder('t')
            ->delete()
            ->where('t.expiresAt < ?1')
            ->setParameters([1 => time()])
            ->getQuery()
            ->execute()
          ;
    }
}
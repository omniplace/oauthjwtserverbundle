<?php

namespace Omni\OAuthJWTServerBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Omni\OAuthJWTServerBundle\Model\AuthCodeInterface;
use Omni\OAuthJWTServerBundle\Model\AuthCodeManagerInterface;

class AuthCodeManager implements AuthCodeManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * AuthCodeManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param string                 $class
     */
    public function __construct(EntityManagerInterface $em, string $class)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $this->class = $class;
    }

    /**
     * Create a new auth code.
     *
     * @return AuthCodeInterface
     */
    public function createAuthCode(): AuthCodeInterface
    {
        return new $this->class;
    }

    /**
     * Return the class name.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Retrieve an auth code using a set of criteria.
     *
     * @param array $criteria
     *
     * @return null|AuthCodeInterface
     */
    public function findAuthCodeBy(array $criteria): ?AuthCodeInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findAuthCodeByToken(string $token): ?AuthCodeInterface
    {
        return $this->findAuthCodeBy(['token' => $token]);
    }

    /**
     * Update a given auth code.
     *
     * @param AuthCodeInterface $authCode
     */
    public function updateAuthCode(AuthCodeInterface $authCode): void
    {
        if (null === $authCode->getId()) {
            $this->em->persist($authCode);
        }

        $this->em->flush();
    }

    /**
     * Delete a given auth code.
     *
     * @param AuthCodeInterface $authCode
     */
    public function deleteAuthCode(AuthCodeInterface $authCode): void
    {
        $this->em->remove($authCode);
        $this->em->flush();
    }

    /**
     * Delete expired auth codes.
     *
     * @return int The number of auth codes deleted.
     */
    public function deleteExpired(): int
    {
        return $this->repository->createQueryBuilder('a')
          ->delete()
          ->where('a.expiresAt < ?1')
          ->setParameters([1 => time()])
          ->getQuery()->execute()
        ;
    }
}
<?php

namespace Omni\OAuthJWTServerBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Omni\OAuthJWTServerBundle\Model\ClientInterface;
use Omni\OAuthJWTServerBundle\Model\ClientManagerInterface;

class ClientManager implements ClientManagerInterface
{
    /**
     * @var $em EntityManagerInterface
     */
    protected $em;

    /**
     * @var $repository EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * ClientManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param string                 $class
     */
    public function __construct(EntityManagerInterface $em, string $class)
    {
        $this->em = $em;
        $this->repository = $em->getRepository($class);
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function createClient(): ClientInterface
    {
        return new $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function findClientBy(array $criteria): ?ClientInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findClientByPublicId(string $publicId): ?ClientInterface
    {
        if (false === $pos = strpos($publicId, '_')) {
            return null;
        }

        $id = substr($publicId, 0, $pos);
        $randomId = substr($publicId, $pos + 1);

        return $this->findClientBy(array(
          'id'       => $id,
          'randomId' => $randomId,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function updateClient(ClientInterface $client): void
    {
        if (null === $client->getId()) {
            $this->em->persist($client);
        }

        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function deleteClient(ClientInterface $client): void
    {
        $this->em->remove($client);
        $this->em->flush();
    }
}
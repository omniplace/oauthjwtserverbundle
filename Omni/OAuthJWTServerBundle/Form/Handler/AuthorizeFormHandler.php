<?php

namespace Omni\OAuthJWTServerBundle\Form\Handler;

use Omni\OAuthJWTServerBundle\Form\Model\Authorize;
use Omni\OAuthJWTServerBundle\Lib\OAuth2;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthorizeFormHandler
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var OAuth2
     */
    protected $server;

    /**
     * AuthorizeFormHandler constructor.
     *
     * @param FormInterface $form
     * @param RequestStack  $requestStack
     * @param OAuth2        $server
     */
    public function __construct(FormInterface $form, RequestStack $requestStack, OAuth2 $server)
    {
        $this->form = $form;
        $this->requestStack = $requestStack;
        $this->server = $server;
    }

    public function isAccepted(): bool
    {
        return $this->form->getData()->accepted;
    }

    public function isRejected(): bool
    {
        return !$this->form->getData()->accepted;
    }

    public function getScope()
    {
        return $this->form->getData()->scope;
    }

    public function process(): bool
    {
        $request = $this->requestStack->getCurrentRequest();

        if (null === $request) {
            return false;
        }

        $this->form->setData(new Authorize(
          $request->request->has('accepted'),
          $request->query->all()
        ));

        if (Request::METHOD_POST !== $request->getMethod()) {
            return false;
        }

        $this->form->handleRequest($request);
        if (!$this->form->isValid()) {
            return false;
        }

        $this->onSuccess();

        return true;
    }

    /**
     * Put
     *
     * @return void
     */
    protected function onSuccess(): void
    {
        $_GET = array(
          'client_id'     => $this->form->getData()->client_id,
          'response_type' => $this->form->getData()->response_type,
          'redirect_uri'  => $this->form->getData()->redirect_uri,
          'state'         => $this->form->getData()->state,
          'scope'         => $this->form->getData()->scope,
        );
    }
}
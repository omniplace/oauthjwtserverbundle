<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\OAuth2;

abstract class Client implements ClientInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $randomId;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var array
     */
    protected $redirectUris = [];

    /**
     * @var array
     */
    protected $allowedGrantTypes = [];

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->allowedGrantTypes[] = OAuth2::GRANT_TYPE_AUTH_CODE;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId(int $id): ClientInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setRandomId(string $random): ClientInterface
    {
        $this->randomId = $random;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRandomId(): ?string
    {
        return $this->randomId;
    }

    /**
     * {@inheritdoc}
     */
    public function setSecret(string $secret): ClientInterface
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function checkSecret($secret): bool
    {
        return null === $this->secret || $secret === $this->secret;
    }

    /**
     * {@inheritdoc}
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * {@inheritdoc}
     */
    public function getPublicId(): string
    {
        return sprintf('%s_%s', $this->getId(), $this->getRandomId());
    }

    /**
     * {@inheritdoc}
     */
    public function getRedirectUris(): array
    {
        return $this->redirectUris;
    }

    /**
     * {@inheritdoc}
     */
    public function setRedirectUris(array $redirectUris): ClientInterface
    {
        $this->redirectUris = $redirectUris;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAllowedGrantTypes(array $grantTypes): ClientInterface
    {
        $this->allowedGrantTypes = $grantTypes;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllowedGrantTypes(): array
    {
        return $this->allowedGrantTypes;
    }
}
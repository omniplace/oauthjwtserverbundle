<?php

namespace Omni\OAuthJWTServerBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;

abstract class Token implements TokenInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var int
     */
    protected $expiresAt;

    /**
     * @var string
     */
    protected $scope;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var string
     */
    protected $grantType;

    /**
     * {@inheritdoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId(int $id): TokenInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientId(): string
    {
        return $this->client->getPublicId();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpiresIn(): int
    {
        return $this->expiresAt ? $this->expiresAt - time() : PHP_INT_MAX;
    }

    /**
     * {@inheritdoc}
     */
    public function hasExpired(): bool
    {
        return $this->expiresAt ? time() > $this->expiresAt : false;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->getUser();
    }

    /**
     * {@inheritdoc}
     */
    public function setExpiresAt(int $timestamp): TokenInterface
    {
        $this->expiresAt = $timestamp;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpiresAt(): int
    {
        return $this->expiresAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setToken(string $token): TokenInterface
    {
        $this->token = $token;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setScope(string $scope): TokenInterface
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(UserInterface $user): TokenInterface
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setClient(ClientInterface $client): TokenInterface
    {
        $this->client = $client;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setGrantType(string $grantType): TokenInterface
    {
        $this->grantType = $grantType;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGrantType(): ?string
    {
        return $this->grantType;
    }
}
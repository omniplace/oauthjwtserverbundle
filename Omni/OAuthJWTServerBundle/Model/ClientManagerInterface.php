<?php

namespace Omni\OAuthJWTServerBundle\Model;

interface ClientManagerInterface
{
    /**
     * @return ClientInterface
     */
    public function createClient(): ClientInterface;

    /**
     * @return string
     */
    public function getClass(): string;

    /**
     * @param array $criteria
     *
     * @return ClientInterface
     */
    public function findClientBy(array $criteria): ?ClientInterface;

    /**
     * @param string $publicId
     *
     * @return ClientInterface
     */
    public function findClientByPublicId(string $publicId): ?ClientInterface;

    /**
     * @param ClientInterface
     */
    public function updateClient(ClientInterface $client): void;

    /**
     * @param ClientInterface
     */
    public function deleteClient(ClientInterface $client): void;
}
<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\IOAuth2GrantClient;
use OAuth2\IOAuth2GrantCode;
use OAuth2\IOAuth2GrantImplicit;
use OAuth2\IOAuth2GrantUser;
use OAuth2\IOAuth2RefreshTokens;
use OAuth2\Model\IOAuth2AuthCode;
use OAuth2\Model\IOAuth2Client;
use OAuth2\Model\IOAuth2Token;

/**
 * Implement all necessary interfaces for work with OAuth2 lib.
 *
 * Added return type for all methods.
 *
 * @package Omni\OAuthJWTServerBundle\Model
 */
interface ExtendedOAuthStorageInterface extends IOAuth2RefreshTokens, IOAuth2GrantUser, IOAuth2GrantCode, IOAuth2GrantImplicit, IOAuth2GrantClient
{
    /**
     * {@inheritdoc}
     */
    public function checkClientCredentialsGrant(IOAuth2Client $client, $clientSecret): bool;

    /**
     * {@inheritdoc}
     */
    public function getAuthCode($code): ?AuthCodeInterface;

    /**
     * {@inheritdoc}
     */
    public function createAuthCode($code, IOAuth2Client $client, $data, $redirectUri, $expires, $scope = null): AuthCodeInterface;

    /**
     * {@inheritdoc}
     */
    public function markAuthCodeAsUsed($code): void;

    /**
     * Small changes in the logic of the parent method: return array in all cases.
     * If array is empty then access was not granted, otherwise username and password are valid.
     *
     * @param IOAuth2Client $client
     * @param string        $username
     * @param string        $password
     *
     * @return array
     */
    public function checkUserCredentials(IOAuth2Client $client, $username, $password): array;

    /**
     * {@inheritdoc}
     */
    public function getRefreshToken($refreshToken): ?TokenInterface;

    /**
     * {@inheritdoc}
     */
    public function createRefreshToken($refreshToken, IOAuth2Client $client, $data, $expires, $scope = null): TokenInterface;

    /**
     * {@inheritdoc}
     */
    public function unsetRefreshToken($refreshToken): void;

    /**
     * {@inheritdoc}
     */
    public function getClient($clientId): ?ClientInterface;

    /**
     * {@inheritdoc}
     */
    public function checkClientCredentials(IOAuth2Client $client, $clientSecret = null): bool;

    /**
     * {@inheritdoc}
     */
    public function getAccessToken($oauthToken): ?TokenInterface;

    /**
     * {@inheritdoc}
     */
    public function createAccessToken($oauthToken, IOAuth2Client $client, $data, $expires, $scope = null): ?TokenInterface;

    /**
     * {@inheritdoc}
     */
    public function checkRestrictedGrantType(IOAuth2Client $client, $grantType): bool;
}
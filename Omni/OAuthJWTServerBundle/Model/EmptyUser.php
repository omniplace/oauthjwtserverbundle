<?php

namespace Omni\OAuthJWTServerBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;

final class EmptyUser implements UserInterface
{

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
    }
}
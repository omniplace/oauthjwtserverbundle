<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\Model\IOAuth2AuthCode;

interface AuthCodeInterface extends TokenInterface, IOAuth2AuthCode
{
    /**
     * @param string $redirectUri
     *
     * @return TokenInterface
     */
    public function setRedirectUri(string $redirectUri): TokenInterface;

    /**
     * @return string
     */
    public function getRedirectUri(): string;
}
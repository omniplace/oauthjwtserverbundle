<?php

namespace Omni\OAuthJWTServerBundle\Model;

abstract class AuthCode extends Token implements AuthCodeInterface
{
    /**
     * @var string
     */
    protected $redirectUri;

    /**
     * @param string $redirectUri
     *
     * @return TokenInterface
     */
    public function setRedirectUri(string $redirectUri): TokenInterface
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }
}
<?php

namespace Omni\OAuthJWTServerBundle\Model;


interface AuthCodeManagerInterface
{
    /**
     * Create a new auth code.
     *
     * @return AuthCodeInterface
     */
    public function createAuthCode(): AuthCodeInterface;

    /**
     * Return the class name.
     *
     * @return string
     */
    public function getClass(): string;

    /**
     * Retrieve an auth code using a set of criteria.
     *
     * @param array $criteria
     *
     * @return null|AuthCodeInterface
     */
    public function findAuthCodeBy(array $criteria): ?AuthCodeInterface;

    /**
     * Retrieve an auth code by its token.
     *
     * @param string $token
     *
     * @return null|AuthCodeInterface
     */
    public function findAuthCodeByToken(string $token): ?AuthCodeInterface;

    /**
     * Update a given auth code.
     *
     * @param AuthCodeInterface $authCode
     */
    public function updateAuthCode(AuthCodeInterface $authCode): void;

    /**
     * Delete a given auth code.
     *
     * @param AuthCodeInterface $authCode
     */
    public function deleteAuthCode(AuthCodeInterface $authCode): void;

    /**
     * Delete expired auth codes.
     *
     * @return int The number of auth codes deleted.
     */
    public function deleteExpired(): int;
}
<?php

namespace Omni\OAuthJWTServerBundle\Model;

abstract class RefreshToken extends Token implements RefreshTokenInterface
{
}
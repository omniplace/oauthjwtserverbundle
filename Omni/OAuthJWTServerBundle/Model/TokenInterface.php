<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\Model\IOAuth2Token;
use Symfony\Component\Security\Core\User\UserInterface;

interface TokenInterface extends IOAuth2Token
{
    /**
     * {@inheritdoc}
     */
    public function getClientId(): string;

    /**
     * {@inheritdoc}
     */
    public function getExpiresIn(): int;

    /**
     * {@inheritdoc}
     */
    public function hasExpired(): bool;

    /**
     * {@inheritdoc}
     */
    public function getToken(): string;

    /**
     * {@inheritdoc}
     */
    public function getScope(): ?string;

    /**
     * {@inheritdoc}
     */
    public function getData();

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     *
     * @return TokenInterface
     */
    public function setId(int $id): self;

    /**
     * @param int $timestamp
     *
     * @return TokenInterface
     */
    public function setExpiresAt(int $timestamp): self;

    /**
     * @return int
     */
    public function getExpiresAt(): int;

    /**
     * @param string $token
     *
     * @return TokenInterface
     */
    public function setToken(string $token): self;

    /**
     * @param string $scope
     *
     * @return TokenInterface
     */
    public function setScope(string $scope): self;

    /**
     * @param UserInterface $user
     *
     * @return TokenInterface
     */
    public function setUser(UserInterface $user): self;

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;

    /**
     * @param ClientInterface $client
     *
     * @return TokenInterface
     */
    public function setClient(ClientInterface $client): self;

    /**
     * @param string $grantType
     *
     * @return TokenInterface
     */
    public function setGrantType(string $grantType): self;

    /**
     * @return null|string
     */
    public function getGrantType(): ?string;
}

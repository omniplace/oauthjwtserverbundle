<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\Model\IOAuth2RefreshToken;

interface RefreshTokenInterface extends TokenInterface, IOAuth2RefreshToken
{
}
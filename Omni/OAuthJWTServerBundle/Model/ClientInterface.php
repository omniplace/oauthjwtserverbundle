<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\Model\IOAuth2Client;

/**
 * Base interface of OAuth client.
 *
 * Interface ClientInterface
 *
 * @package OAuthJWTServerBundle\Model
 */
interface ClientInterface extends IOAuth2Client
{

    /**
     * {@inheritdoc}
     */
    public function getPublicId(): string;

    /**
     * {@inheritdoc}
     */
    public function getRedirectUris(): array;

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param int $id
     *
     * @return ClientInterface
     */
    public function setId(int $id): self;

    /**
     * @param string $random
     *
     * @return ClientInterface
     */
    public function setRandomId(string $random): self;

    /**
     * @return string|null
     */
    public function getRandomId(): ?string;

    /**
     * @param string $secret
     *
     * @return ClientInterface
     */
    public function setSecret(string $secret): self;

    /**
     * @param $secret
     *
     * @return bool
     */
    public function checkSecret($secret): bool;

    /**
     * @return string
     */
    public function getSecret(): string;

    /**
     * @param array $redirectUris
     *
     * @return ClientInterface
     */
    public function setRedirectUris(array $redirectUris): self;

    /**
     * @param array $grantTypes
     *
     * @return ClientInterface
     */
    public function setAllowedGrantTypes(array $grantTypes): self;
    /**
     * @return array
     */
    public function getAllowedGrantTypes(): array;
}
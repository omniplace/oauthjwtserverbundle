<?php

namespace Omni\OAuthJWTServerBundle\Model;

interface TokenManagerInterface
{
    /**
     * Create a new TokenInterface.
     *
     * @return TokenInterface
     */
    public function createToken(): TokenInterface;

    /**
     * Return the class name of the Token.
     *
     * @return string
     */
    public function getClass(): string;

    /**
     * Retrieve a token using a set of criteria.
     *
     * @param array $criteria
     *
     * @return TokenInterface|null
     */
    public function findTokenBy(array $criteria): ?TokenInterface;

    /**
     * Retrieve a token (object) by its token string.
     *
     * @param string $token A token.
     *
     * @return TokenInterface|null
     */
    public function findTokenByToken(string $token): ?TokenInterface;

    /**
     * Save or update a given token.
     *
     * @param TokenInterface $token The token to save or update.
     */
    public function updateToken(TokenInterface $token): void;

    /**
     * Delete a given token.
     *
     * @param TokenInterface $token The token to delete.
     */
    public function deleteToken(TokenInterface $token): void;

    /**
     * Delete expired tokens.
     *
     * @return int The number of tokens deleted.
     */
    public function deleteExpired(): int;
}
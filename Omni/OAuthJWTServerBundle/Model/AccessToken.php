<?php

namespace Omni\OAuthJWTServerBundle\Model;

use OAuth2\Model\IOAuth2AccessToken;

/**
 * Class AccessToken
 * @package Omni\OAuthJWTServerBundle\Model
 */
final class AccessToken extends Token implements IOAuth2AccessToken
{
}
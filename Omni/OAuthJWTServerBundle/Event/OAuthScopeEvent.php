<?php

namespace Omni\OAuthJWTServerBundle\Event;

use Symfony\Component\Security\Core\User\UserInterface;

class OAuthScopeEvent
{
    public const CHECK_SCOPE_ACCESS = 'omni_oauth_jwt_server.on_check_scope_access';

    /**
     * @var string
     */
    private $availableScope;

    /**
     * @var string
     */
    private $requiredScope;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * OAuthScopeEvent constructor.
     *
     * @param string             $availableScope
     * @param string             $requiredScope
     * @param UserInterface|null $user
     */
    public function __construct(string $availableScope, string $requiredScope, UserInterface $user = null)
    {
        $this->availableScope = $availableScope;
        $this->requiredScope = $requiredScope;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getAvailableScope(): string
    {
        return $this->availableScope;
    }

    /**
     * @return string
     */
    public function getRequiredScope(): string
    {
        return $this->requiredScope;
    }
}
<?php

namespace Omni\OAuthJWTServerBundle\Event;

use Omni\OAuthJWTServerBundle\Model\AccessToken;

class OAuthPayloadEvent
{
    public const ADD_PAYLOAD_EVENT = 'omni_oauth_jwt_server.on_add_payload_event';

    public const EXTRACT_PAYLOAD_EVENT = 'omni_oauth_jwt_server.on_extract_payload_event';

    /**
     * @var array
     */
    private $payload;

    /**
     * @var AccessToken
     */
    private $token;

    /**
     * OAuthPayloadEvent constructor.
     *
     * @param array $payload
     * @param AccessToken $token
     */
    public function __construct(array $payload, AccessToken $token = null)
    {
        $this->payload = $payload;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @return AccessToken
     */
    public function getToken(): AccessToken
    {
        return $this->token;
    }

    /**
     * @param AccessToken $token
     */
    public function setToken(AccessToken $token): void
    {
        $this->token = $token;
    }
}
<?php

namespace Omni\OAuthJWTServerBundle;

use Omni\OAuthJWTServerBundle\DependencyInjection\Compiler\TokenStorageCompilerPass;
use Omni\OAuthJWTServerBundle\DependencyInjection\Security\Factory\OAuthFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OmniOAuthJWTServerBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        /** @var SecurityExtension $extension */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new OAuthFactory());

        $container->addCompilerPass(new TokenStorageCompilerPass());
    }

}
